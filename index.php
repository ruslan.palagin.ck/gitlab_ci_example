<?php

/**
 * Technical Specification:
 * 1. Output MUST contain "Welcome message"
 * 2. "Welcome message" MUST contain word "Hello"
 * 3. "Welcome message" MUST contain word "CI"
 */

/**
 * Task #2
 * 1. Replace "!" after "CI" with "."
 */
echo 'Hello CI. Happy new year =)' . PHP_EOL;

